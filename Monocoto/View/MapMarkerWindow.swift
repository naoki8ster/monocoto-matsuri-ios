//
//  MapMarkerWindow.swift
//  Monocoto
//
//  Created by 山崎 直樹 on 2018/07/14.
//  Copyright © 2018年 naoki.yamazaki. All rights reserved.
//

import UIKit

protocol MapMarkerDelegate: class {
    func didTapInfoButton(data: NSDictionary)
}

class MapMarkerWindow: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var shopNameLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!

    weak var delegate: MapMarkerDelegate?
    var shopData: NSDictionary?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.genreLabel.layer.cornerRadius = 7
        self.genreLabel.clipsToBounds = true
        
    }
    
    @IBAction func didTapInfoButton(_ sender: UIButton) {
        delegate?.didTapInfoButton(data: shopData!)
    }
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "MapMarkerWindow", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }

}
