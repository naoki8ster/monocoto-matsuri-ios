//
//  MapViewController.swift
//  Monocoto
//
//  Created by 山崎 直樹 on 2018/06/24.
//  Copyright © 2018年 naoki.yamazaki. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import GoogleMaps

class MapViewController: UIViewController, GMSMapViewDelegate, MapMarkerDelegate, CLLocationManagerDelegate {
  
    private var mapView: GMSMapView!
    private var infoWindow = MapMarkerWindow()
    fileprivate var locationMarker : GMSMarker? = GMSMarker()
    var locationManager: CLLocationManager!
    
    var mapDefLatitude: Double?
    var mapDefLongtitude: Double?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.infoWindow = loadNiB()
        
        //位置情報サービスが利用できるかを確認
        CLLocationManager.locationServicesEnabled()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        locationManager.requestAlwaysAuthorization()
        
    }

    //画面表示後の呼び出しメソッド
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //ヘッダタイトル
        self.parent!.parent!.navigationItem.title = Const.MapHeaderName

        if(CLLocationManager.locationServicesEnabled() == true){
            print("debug: location-true")
        }

    }
    
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!){
        let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude:newLocation.coordinate.latitude,longitude:newLocation.coordinate.longitude)
        let now :GMSCameraPosition = GMSCameraPosition.camera(withLatitude: coordinate.latitude,longitude:coordinate.longitude,zoom:18)
        self.mapView.camera = now
        
    }
    
    func didTapInfoButton(data: NSDictionary) {

        let shopName = data["shopname"] as? String
        
        //
        let navBar = UINavigationBar()
        navBar.isTranslucent = false
        navBar.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 150)
        
        //
        let navItem = UINavigationItem(title: shopName!)
        navItem.rightBarButtonItem = UIBarButtonItem(title:"閉じる", style:UIBarButtonItemStyle.plain, target:self, action: #selector(action(sender:)))
        
        //
        self.navigationItem.setRightBarButton(navItem.rightBarButtonItem, animated: true)
        navBar.pushItem(navItem, animated: true)
        
        //
        let shopDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShopDetail") as! ShopDetailViewController
        shopDetailViewController.shop = ShopData(snapshot:data)
        shopDetailViewController.view.addSubview(navBar)

        self.present(shopDetailViewController, animated: true, completion: nil)

    }
    
    @objc func action(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadNiB() -> MapMarkerWindow {
        let infoWindow = MapMarkerWindow.instanceFromNib() as! MapMarkerWindow
        return infoWindow
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func loadView() {

        //中心
        let camera = GMSCameraPosition.camera(withLatitude: Const.MapDefLatitude, longitude: Const.MapDefLongtitude, zoom: Float(Double(Const.MapDefZoom)))
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.view = self.mapView

    }

    //マーカータップ時インフォウィンドウを表示
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        var markerData : NSDictionary?
        if let data = marker.userData! as? NSDictionary {
            markerData = data
        }

        locationMarker = marker
        infoWindow.removeFromSuperview()
        infoWindow = loadNiB()
        guard let location = locationMarker?.position else {
            print("locationMarker is nil")
            return false
        }
    
        infoWindow.shopData = markerData
        infoWindow.delegate = self
        //UI
        infoWindow.alpha = 1
        infoWindow.layer.cornerRadius = 12
        infoWindow.layer.borderWidth = 1
        infoWindow.layer.borderColor = UIColor.darkGray.cgColor
        infoWindow.infoButton.layer.cornerRadius = infoWindow.infoButton.frame.height / 2
        
        //let id = markerData!["id"]!
        //print("debug: id \(id))")
        
        let shopName = markerData!["shopname"]!
        let genre = markerData!["genre"]!
        let imageNames = markerData!["images"]! as? [String]
        
        //1枚目の写真を表示
        if let imageName = imageNames?[0] {
            let storage = Storage.storage()
            let storageRef = storage.reference()
            let spaceRef = storageRef.child("shop/" + imageName)
            infoWindow.imageView.sd_setImage(with: spaceRef)
        }
        
        infoWindow.shopNameLabel.text = shopName as? String
        infoWindow.genreLabel.text = genre as? String
        
        infoWindow.center = mapView.projection.point(for: location)
        infoWindow.center.y = infoWindow.center.y - 30
        
        self.view.addSubview(infoWindow)
        //print("You tapped \(shopName)")
        return false
        
    }

    //マップが移動されたらインフォウィンドウが付いて行く
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if (locationMarker != nil) {
            guard let location = locationMarker?.position else {
                print("locationMarker is nil")
                return
            }
            infoWindow.center = mapView.projection.point(for: location)
            infoWindow.center.y = infoWindow.center.y - 30
        }
    }
    
    //マップどこか押すとインフォウィンドウを消す
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        infoWindow.removeFromSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        //設定から地図表示する・しないを判断
        let settingRef = Database.database().reference().child(Const.SettingPath)
        settingRef.observe(.childAdded, with: {snapshot in
          let settingData = SettingData(snapshot: snapshot)
          if let isMarkerAppear = settingData.isMarkerAppear {
            print("debug: marker : \(isMarkerAppear)")
            if isMarkerAppear == true {
              self.setMarker()
            }
          }
        })

    }

    func setMarker(){
        //出店情報参照
        let shopRef = Database.database().reference().child(Const.ShopPath)
    
        //要素追加
        shopRef.observe(.childAdded, with: {snapshot in
            if snapshot.value as? [String: AnyObject] != nil {
                //初期化
                self.mapView.clear()
                guard let shopData = snapshot.value as? [String: AnyObject] else {
                    return
                }
                
                //マーカー設定
                if let latitude = shopData["latitude"] as? Double, let longtitude = shopData["longtitude"] as? Double {
                    DispatchQueue.main.async(execute:{
                        let marker = GMSMarker()
                        
                        //print("debug: \(String(describing: shopData["category"]!))")
                        //カテゴリからマーカーを設定する
                        if let category = shopData["category"] as? Int {
                            switch category {
                                case 1:
                                    marker.icon = ShopCategory.buppan.getIconImage()
                                case 3:
                                    marker.icon = ShopCategory.eat.getIconImage()
                                case 4:
                                    marker.icon = ShopCategory.workshop.getIconImage()
                                case 5:
                                    marker.icon = ShopCategory.jimoto_tempo.getIconImage()
                                case 6:
                                    marker.icon = ShopCategory.jyumin_group.getIconImage()
                                //case 7:
                                //    marker.icon = ShopCategory.music.getIconImage()
                                case 8:
                                    marker.icon = ShopCategory.jimoto_tempo_eat.getIconImage()
                                default:
                                    marker.icon = UIImage(named: "other")!
                            }
                            
                        }
                        else{
                            marker.icon = UIImage(named: "other")!
                        }
                        marker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longtitude)
                        marker.userData = shopData
                        marker.map = self.mapView
                        
                    })
                }
            }
        })
        
    }
  
  
}
