//
//  ShopViewController.swift
//  Monocoto
//
//  Created by 山崎 直樹 on 2018/06/24.
//  Copyright © 2018年 naoki.yamazaki. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class ShopViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
    @IBOutlet weak var tableView: UITableView!

    var shopArray: [ShopData] = [] //お店情報表示用配列
  
    override func viewDidLoad() {
        super.viewDidLoad()

        //デリゲート
        tableView.delegate = self
        tableView.dataSource = self
    
        let nib = UINib(nibName: "ShopTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "Cell")
    
        //テーブル高さ
        tableView.rowHeight = 150
    
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //セルを取得してデータを設定する
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ShopTableViewCell
        cell.setPostData(shopArray[indexPath.row])
        return cell
    
    }
  
    //delegete(tableview)
    //出店者詳細へ遷移する
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //ナビゲーションバー
        let navBar = UINavigationBar()
        navBar.isTranslucent = false
        navBar.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 150)
        
        //ナビゲーションアイテム(出店者名と閉じるボタン)
        let shopName = shopArray[indexPath.row].shopname
        let navItem = UINavigationItem(title: shopName!)
        navItem.rightBarButtonItem = UIBarButtonItem(title:"閉じる", style:UIBarButtonItemStyle.plain, target:self, action: #selector(action(sender:)))
        
        
        //ナビゲーションバーにアイテムを追加
        self.navigationItem.setRightBarButton(navItem.rightBarButtonItem, animated: true)
        navBar.pushItem(navItem, animated: true)
        
        //モーダル
        let shopDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShopDetail") as! ShopDetailViewController
        shopDetailViewController.shop = shopArray[indexPath.row]
        
        //ナビゲーションバー追加
        shopDetailViewController.view.addSubview(navBar)
        
        //表示
        self.present(shopDetailViewController, animated: true, completion: {
            //self.tableView.reloadData()
            //self.tableView.deselectRow(at: indexPath, animated: true)
            //self.tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.none, animated: true)
            //self.tableView.setContentOffset(.zero, animated: true)
            //self.tableView.scrollToNearestSelectedRow(at: UITableViewScrollPosition.middle, animated: true)
            print("debug\(indexPath)")
        })
        
        
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        //一度クリア
        shopArray.removeAll()
    
        //お店情報表示
        let shopRef = Database.database().reference().child(Const.ShopPath)
        
        //要素追加
        shopRef.observe(.childAdded, with: {snapshot in
            
          //ShopDataクラスを呼び出しデータ設定
          let shopData = ShopData(snapshot: snapshot)
          self.shopArray.insert(shopData, at: 0)
          
          //TableView再表示
          self.tableView.reloadData()
          
        })
    
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent!.parent!.navigationItem.title = Const.ShopHeaderName
        
    }

    //閉じるボタン
    @objc func action(sender: UIButton){
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
