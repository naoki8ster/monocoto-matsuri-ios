//
//  ViewController.swift
//  Monocoto
//
//  Created by 山崎 直樹 on 2018/06/24.
//  Copyright © 2018年 naoki.yamazaki. All rights reserved.
//

import UIKit
import ESTabBarController
import Firebase
import FirebaseDatabase
import SafariServices

class ViewController: UIViewController {

    var aboutPage: String?
    var isMapAppear: Bool?
    var esTabBarController : ESTabBarController!
    var shopImgPath: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isMapAppear = false
        self.aboutPage = "http://monocoto-matsuri.com/"
        
        let settingRef = Database.database().reference().child(Const.SettingPath)
        settingRef.observe(.childAdded, with: {snapshot in
            let settingData = SettingData(snapshot: snapshot)
            //設定から地図表示する・しないを判断
            if let isMapAppear = settingData.isMapAppear {
                self.isMapAppear = isMapAppear
                self.setupTab()
            }
            //アバウトページのURLを取得
            if let aboutPage = settingData.aboutPage {
                self.aboutPage = aboutPage
            }
            
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //アバウトアイコンを押下
    @IBAction func buttonAbout(_ sender: Any) {

        let safariVC = SFSafariViewController(url: NSURL(string: self.aboutPage!)! as URL)
        safariVC.dismissButtonStyle = .close
        present(safariVC, animated: true, completion: nil)
        
    }
    
    //下部タブバーを設定
    func setupTab() {
        // ESTabBarControllerを作成
        if self.isMapAppear! {
            esTabBarController = ESTabBarController(tabIconNames: ["shop", "map"])
        } else {
            esTabBarController = ESTabBarController(tabIconNames: ["shop"])
        }
        
        // 背景色、選択時の色を設定
        esTabBarController.selectedColor = UIColor(red: 42/255, green: 168/255, blue: 225/255, alpha: 1)
        esTabBarController.buttonsBackgroundColor = UIColor(red: 239/255, green: 244/255, blue: 244/255, alpha: 1)
        //esTabBarController.buttonsBackgroundColor = UIColor.lightGray
        // タブの高さ
        esTabBarController.selectionIndicatorHeight = 2
        
        // 追加
        addChildViewController(esTabBarController)
        let tabBarView = esTabBarController.view!
        tabBarView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tabBarView)
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            tabBarView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            tabBarView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor),
            tabBarView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
            tabBarView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor)
        ])
        esTabBarController.didMove(toParentViewController: self)
    
        //ビューコントローラを設定
        let shopViewController = storyboard?.instantiateViewController(withIdentifier: "Shop")
        let mapViewController = storyboard?.instantiateViewController(withIdentifier: "Map")

        //マップを表示するかどうか
        if self.isMapAppear! {
            esTabBarController.setView(shopViewController, at: 0)
            esTabBarController.setView(mapViewController, at: 1)
        } else {
            esTabBarController.setView(shopViewController, at: 0)
        }
        
        //初期選択
        esTabBarController.setSelectedIndex(0, animated: true)
        
        
    }
  
}

