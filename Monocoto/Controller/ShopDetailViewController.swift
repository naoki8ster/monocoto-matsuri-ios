//
//  ShopDetailViewController.swift
//  Monocoto
//
//  Created by 山崎 直樹 on 2018/06/29.
//  Copyright © 2018年 naoki.yamazaki. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseStorageUI
import SafariServices

class ShopDetailViewController: UIViewController {

    @IBOutlet weak var shopNameLabel: UILabel!
    @IBOutlet weak var shopImageView: UIImageView!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var catchCopyTextField: UILabel!
    @IBOutlet weak var introLabel: UILabel!
    @IBOutlet weak var recommendLabel: UILabel!
    @IBOutlet weak var recommendPLabel: UILabel!
    @IBOutlet weak var homepageLabel: UILabel!
    @IBOutlet weak var facebookLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var shop: ShopData!
    let cardWidth: CGFloat = 300
    let cardHeight: CGFloat = 300
    var cardViews: [UIView] = []

    func closeBarButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        self.title = shop.shopname
        self.navigationItem.title = self.title
        
    }
    
    //ステータスバー非表示
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ジャンル
        self.genreLabel.text = shop.genre
        self.genreLabel.layer.cornerRadius = 3
        self.genreLabel.clipsToBounds = true

        //キャッチコピー
        self.catchCopyTextField.text = shop.catchcopy

        //イントロ
        self.introLabel.text = shop.intro

        //オススメ
        self.recommendLabel.text = shop.recommend

        //オススメポイント
        self.recommendPLabel.text = shop.recommend_point

        //ホームページ
        self.homepageLabel.text = shop.homePage

        //Facebook
        self.facebookLabel.text = shop.facebook

        //ホームページのリンク
        if let linkText = shop.homePage {

            homepageLabel.isUserInteractionEnabled = true
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGesture))
            homepageLabel.addGestureRecognizer(tapGestureRecognizer)
          
            let string = linkText
            let range = (string as NSString).range(of: linkText)
            let attributedString = NSMutableAttributedString(string: string)
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: range)
            homepageLabel.attributedText = attributedString
          
        }
        
        //フェースブックのリンク
        if let linkTextFb = shop.facebook {
          
            facebookLabel.isUserInteractionEnabled = true
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapGesture_Fb))
            facebookLabel.addGestureRecognizer(tapGestureRecognizer)
          
            let string = linkTextFb
            let range = (string as NSString).range(of: linkTextFb)
            let attributedString = NSMutableAttributedString(string: string)
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: range)
            facebookLabel.attributedText = attributedString
          
        }

        //イメージ
        if shop.images.count > 0 {
            initScrollView(images: shop.images)
        }
    }
    
    @objc func tapGesture(gestureRecognizer: UITapGestureRecognizer){

        guard let text = homepageLabel.text else { return }
        //タップジェスチャ
        let touchPoint = gestureRecognizer.location(in: homepageLabel)
        let textStorage = NSTextStorage(attributedString: NSAttributedString(string: text))
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: homepageLabel.frame.size)
        layoutManager.addTextContainer(textContainer)
        textContainer.lineFragmentPadding = 0
        let toRange = (text as NSString).range(of: text)
        let glyphRange = layoutManager.glyphRange(forCharacterRange: toRange, actualCharacterRange: nil)
        let glyphRect = layoutManager.boundingRect(forGlyphRange: glyphRange, in: textContainer)
        //リンクを開く
        if glyphRect.contains(touchPoint) {
            let url = URL(string: text)
            if let url = url {
                let safariVC = SFSafariViewController(url: url)
                safariVC.dismissButtonStyle = .close
                present(safariVC, animated: true, completion: nil)
            }
        }

    }
  
    @objc func tapGesture_Fb(gestureRecognizer: UITapGestureRecognizer) {

        guard let text = facebookLabel.text else { return }
        //タップジェスチャ
        let touchPoint = gestureRecognizer.location(in: facebookLabel)
        let textStorage = NSTextStorage(attributedString: NSAttributedString(string: text))
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: facebookLabel.frame.size)
        layoutManager.addTextContainer(textContainer)
        textContainer.lineFragmentPadding = 0
        let toRange = (text as NSString).range(of: text)
        let glyphRange = layoutManager.glyphRange(forCharacterRange: toRange, actualCharacterRange: nil)
        let glyphRect = layoutManager.boundingRect(forGlyphRange: glyphRange, in: textContainer)
        //リンクを開く
        if glyphRect.contains(touchPoint) {
            let url = URL(string: text)
            if let url = url {
                let safariVC = SFSafariViewController(url: url)
                safariVC.dismissButtonStyle = .close
                present(safariVC, animated: true, completion: nil)
            }
        }

    }
  
    private func initScrollView(images: [String] ) {

        for i in 0 ... images.count - 1 {
            //print("Debug: images \(i) name=\(images[i])")
            cardViews.append(createCardVidwFromImageName(imageName: images[i], index: i))
        }
        cardViews.forEach {
            scrollView.addSubview($0)
        }
        //枚数分横サイズ指定
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(cardViews.count), height: cardHeight)

    }
  
    private func createCardVidwFromImageName(imageName: String, index: Int) -> UIView {
    
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: cardWidth, height: cardHeight))        
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let spaceRef = storageRef.child("shop/" + imageName)
        imageView.sd_setImage(with: spaceRef)

        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.center.x = view.frame.width / 2
        let cardX: CGFloat = CGFloat(index) * view.frame.width
        let cardView = UIView(frame: CGRect(x:cardX, y:0, width:view.frame.width, height:cardHeight))
        cardView.addSubview(imageView)
        return cardView
    
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
}
