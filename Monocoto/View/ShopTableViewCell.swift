//
//  ShopTableViewCell.swift
//  Monocoto
//
//  Created by 山崎 直樹 on 2018/06/27.
//  Copyright © 2018年 naoki.yamazaki. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseStorageUI

class ShopTableViewCell: UITableViewCell {

    @IBOutlet weak var shopnameLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var catchcopyLabel: UILabel!
    @IBOutlet weak var shopImageView: UIImageView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        self.genreLabel.layer.cornerRadius = 7
        self.genreLabel.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  
    func setPostData(_ shopData: ShopData){
        
        //出店者名
        self.shopnameLabel.text = shopData.shopname

        //ジャンル
        self.genreLabel.text = shopData.genre
        
        //キャッチコピー
        self.catchcopyLabel.text = shopData.catchcopy

        //画像
        if shopData.images.count > 0 {
            //print("debug:\(shopData.shopname)")
            if let image = shopData.images[0] as String? {
                let storage = Storage.storage()
                let storageRef = storage.reference()
                let spaceRef = storageRef.child("shop/" + image)
                shopImageView?.sd_setImage(with: spaceRef)
            }
        }
        
    }
  
  
    
}
