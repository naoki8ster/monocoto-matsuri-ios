//
//  SettingData.swift
//  Monocoto
//
//  Created by 山崎 直樹 on 2018/07/08.
//  Copyright © 2018年 naoki.yamazaki. All rights reserved.
//

import Firebase
import FirebaseDatabase

class SettingData: NSObject {
  
    var id: String?
    var isMapAppear: Bool?
    var isMarkerAppear: Bool?
    var aboutPage: String?
    var mapDefLatidude: Double?
    var mapDefLongtitude: Double?
  
    init(snapshot: DataSnapshot) {
    
        //id
        self.id = snapshot.key
        let valueDictionary = snapshot.value as! [String: Any]
    
        //マップ画面を表示する
        self.isMapAppear = valueDictionary["isMapAppear"] as? Bool
        
        //マーカーを表示する
        self.isMarkerAppear = valueDictionary["isMarkerAppear"] as? Bool
    
        //アバウトページ
        self.aboutPage = valueDictionary["aboutPage"] as? String
        
        //
        self.mapDefLatidude = valueDictionary["mapDefLatidude"] as? Double
        
        //
        self.mapDefLongtitude = valueDictionary["mapDefLongtitude"] as? Double
        
  }
  
}
