//
//  Const.swift
//  Monocoto
//
//  Created by 山崎 直樹 on 2018/06/28.
//  Copyright © 2018年 naoki.yamazaki. All rights reserved.
//

import Foundation
import UIKit

struct Const {
    //設定情報
    static let SettingPath = "setting"
    //出店情報
    static let ShopPath = "shop"
    //出店情報ヘッダ名
    static let ShopHeaderName = "出店者一覧"
    //マップヘッダ名
    static let MapHeaderName = "マップ"
    //
    static let MapDefLatitude = 35.658028
    //
    static let MapDefLongtitude = 139.660101
    //
    static let MapDefZoom = 17.0
}

enum ShopCategory: Int {
    //物販
    case buppan = 1
    //飲食
    case eat = 3
    //子供ワークショップ
    case workshop = 4
    //地元参加店舗
    case jimoto_tempo = 5
    //地元住民グループ
    case jyumin_group = 6
    //音楽
    //case music = 7
    //
    case jimoto_tempo_eat = 8
    
    //マーカーイメージを取得
    func getIconImage() -> UIImage {
        switch self {
            case .buppan:
                return UIImage(named: "buppan")!
            case .eat:
                return UIImage(named: "eat")!
            case .workshop:
                return UIImage(named: "workshop")!
            case .jimoto_tempo:
                return UIImage(named: "jimoto_tempo")!
            case .jimoto_tempo_eat:
                return UIImage(named: "jimoto_tempo_eat")!
            case .jyumin_group:
                return UIImage(named: "jyumin_group")!
            //case .music:
            //    return UIImage(named: "music")!
            
        }
    }
    
}
