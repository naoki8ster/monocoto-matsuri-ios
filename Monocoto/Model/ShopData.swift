//
//  ShopData.swift
//  Monocoto
//
//  Created by 山崎 直樹 on 2018/06/27.
//  Copyright © 2018年 naoki.yamazaki. All rights reserved.
//

import Firebase
import FirebaseDatabase

class ShopData: NSObject {
  
    var id: String?
    var category: Int?
    var shopname: String?
    var genre: String?
    var catchcopy: String?
    var intro: String?
    var recommend: String?
    var recommend_point: String?
    var homePage: String?
    var facebook: String?
    var latitude: Double?
    var longtitude: Double?
    var images: [String] = []
  
    init(snapshot: DataSnapshot) {
    
        //id
        self.id = snapshot.key
        let valueDictionary = snapshot.value as! [String: Any]

        //カテゴリー
        self.category = valueDictionary["category"] as? Int

        //出店者名
        self.shopname = valueDictionary["shopname"] as? String

        //ジャンル
        self.genre = valueDictionary["genre"] as? String

        //キャッチコピー
        self.catchcopy = valueDictionary["catchcopy"] as? String

        //イントロ
        self.intro = valueDictionary["intro"] as? String

        //オススメ
        self.recommend = valueDictionary["recommend"] as? String

        //オススメポイント
        self.recommend_point = valueDictionary["recommend_point"] as? String
        
        //ホームページ
        self.homePage = valueDictionary["homepage"] as? String

        //フェースブック
        self.facebook = valueDictionary["facebook"] as? String

        //緯度
        self.latitude = valueDictionary["latitude"] as? Double
        
        //経度
        self.longtitude = valueDictionary["longtitude"] as? Double

        //画像
        if let images = valueDictionary["images"] as? [String] {
            self.images = images
        }
    
    }
    
    init(snapshot: NSDictionary) {
        
        self.category = snapshot["category"] as? Int
        self.shopname = snapshot["shopname"] as? String
        self.genre = snapshot["genre"] as? String
        self.catchcopy = snapshot["catchcopy"] as? String
        self.intro = snapshot["intro"] as? String
        self.recommend = snapshot["recommend"] as? String
        self.recommend_point = snapshot["recommend_point"] as? String
        self.homePage = snapshot["homepage"] as? String
        self.facebook = snapshot["facebook"] as? String
        self.latitude = snapshot["latitude"] as? Double
        self.longtitude = snapshot["longtitude"] as? Double
        if let images = snapshot["images"] as? [String] {
            self.images = images
        }
        
    }
    
    
  
}
